import React, { useEffect, useState } from 'react';
import { FaBars, FaTimes, FaUser  } from 'react-icons/fa';
import '../styles/Navbar.css';
import logoNaranja from '../assets/logoNaranja.png';
import MenuPrincipal from './MenuPrincipal';
import Menu from './Menu';
import { getAuth, onAuthStateChanged } from 'firebase/auth';
import { app } from '../api/firebaseConfig';

const Navbar = ({ onMenuToggle }) => {

  const [user, setUser] = useState(null);

  useEffect(() => {
    const auth = getAuth(app);
    const unsubscribe = onAuthStateChanged(auth, (user) => {
      setUser(user);
    });

    return () => unsubscribe(); 
  }, [])

  const [menuOpen, setMenuOpen] = useState(false);

  const handleMenuToggle = () => {
    setMenuOpen(!menuOpen);
    onMenuToggle(); 
  };

  const handleCloseMenu = () => { 
    setMenuOpen(false);
  };

  const [menuOpenInicio, setMenuOpenInicio] = useState(false);

  const handleMenuToggleInicio = () => {
    setMenuOpenInicio(!menuOpenInicio);
  };

  const handleCloseMenuInicio = () => {
    setMenuOpenInicio(false);
  };

  return (
    <nav className={`main-header-welcome ${menuOpen ? 'menu-open-welcome' : ''} navbar ${menuOpenInicio ? 'menu-open' : ''}`}>
      <div className="header-left-welcome">
        <div className="menu-icon-welcome" onClick={handleMenuToggle}>
          {menuOpen ? <FaTimes /> : <FaBars />}
        </div>
        { user ? (
          <>
          <ul> 
            <a href="/terratrak/Welcome">
              <li>
                  <img src={logoNaranja} alt="Logo" className="app-logo-navbar" />
              </li>
            </a>
          </ul>
            <a href="/terratrak/Welcome">
              <h1 className="app-title-welcome">Terratrack</h1>
            </a>
          </>
       ) : (
        <>
          <ul> 
            <a href="/">
              <li>
                  <img src={logoNaranja} alt="Logo" className="app-logo-navbar" />
              </li>
            </a>
          </ul>
            <a href="/">
              <h1 className="app-title-welcome">Terratrack</h1>
            </a>
        </>
        )}
      </div>
      <div className="navbar-right">
        <ul>
          <div className="menu-icon" onClick={handleMenuToggleInicio}>
            <FaBars />
          </div>
          {/* Muestra el componente Menu si el menú está abierto */}
          {menuOpenInicio && <Menu isOpen={menuOpenInicio} onClose={handleCloseMenuInicio} />}
        </ul>
      </div>
      <div className={`header-right-welcome ${menuOpen ? 'menu-open-welcome' : ''} `}>
        {!user && (
          <ul>
            <li className="user-display-welcome ">
              <a className="ocultar" href="/terratrak/IniciarSesion">
                Iniciar sesión
              </a>
            </li>
          </ul>
        )}
        {!user && (
          <ul>
            <li className="user-display-welcome">
              <a className="ocultar" href="/terratrak/Registro">
                Registrarse
              </a>
            </li>
          </ul>
        )}
        {user &&
        (<ul>  
            <li>
            <div className="user-display-welcome">
                <FaUser className="user-icon" />
                {user.email}
            </div>
            </li>
        </ul>
        )}
      </div>
      {menuOpen && <MenuPrincipal isOpen={menuOpen} onClose={handleCloseMenu} user={user} />}
    </nav>
  );
};

export default Navbar;
