import React from 'react';
import { Link } from 'react-router-dom';
import '../styles/CardMisTerrenos.css'
import { app } from '../api/firebaseConfig';
import { getFirestore, updateDoc, doc, getDoc } from 'firebase/firestore';

const CardMisTerrenos = ({ nombreTerreno, urlFoto, precioTerreno, descripcion, ubicacion, id }) => {

  const handleVendido = async () => {
    const db = getFirestore(app);
    const terrenoRef = doc(db, 'terrenos', id);
  
    try {
  
      const terrrenoDoc = await getDoc(terrenoRef);

      if (terrrenoDoc.exists() && terrrenoDoc.data().activo) {
        await updateDoc(terrenoRef, {
          activo: false,
        });
        alert('Terreno vendido con éxito')
      } else {
        alert('Terreno ya vendido')
      }

    } catch (error) {
      alert('Error al vender el terreno', error)
    }
  }

  return (
    <div className="terreno-card-misterrenos">
      <div className="imagen-izquierda-misterrenos">
        <img src={urlFoto} alt={`Foto de ${nombreTerreno}`} />
      </div>
      <div className="info-derecha-misterrenos">
        <div className="titulo-y-favorito-misterrenos">
          <h2>{nombreTerreno}</h2>
        </div>
        <Link to={`/terratrak/DetalleTerreno/${id}`} className="terreno-info-misterrenos">
          <p>Precio: ${precioTerreno}</p>
          <p>{descripcion}</p>
        </Link>
        <div className="botones-container-misterrenos">
          <Link to={`/terratrak/EditarTerreno/${id}`} className="editar-link-misterrenos">
            <button className="editar-misterrenos">Editar</button>
          </Link>
          <button className="vendido-misterrenos" onClick={handleVendido} >Vendido</button>
        </div>
      </div>
    </div>
  );
};

export default CardMisTerrenos;