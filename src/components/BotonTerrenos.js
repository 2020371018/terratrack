// BotonTerrenos.jsx
import React from 'react';
import PropTypes from 'prop-types';
import '../styles/BotonTerrenos.css';

const BotonTerrenos = ({ texto, tamano, color }) => {
  const style = {
    backgroundColor: color, 
  };

  return (
    <button style={style} className={`mi-boton ${tamano}`}>
      {texto}
    </button>
  );
};

BotonTerrenos.propTypes = {
  texto: PropTypes.string.isRequired,
  tamano: PropTypes.string,
  color: PropTypes.string,
};

export default BotonTerrenos;
