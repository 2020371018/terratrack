import React from 'react';
import '../styles/Footer.css';

const Footer = () => {
  return (
    <footer className='footer'>
      <div className='footer-content'>
        <p>&copy; 2023 Terratrack. Todos los derechos reservados.</p>
      </div>
    </footer>
  );
};

export default Footer;
