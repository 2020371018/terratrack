import React from 'react';
import { Link } from 'react-router-dom';
import '../styles/MenuPrincipal.css';
import { getAuth, signOut } from 'firebase/auth';

const MenuPrincipal = ({ isOpen, onClose, user }) => {

  const handleLogOut = async () => {
    try {
      const auth = getAuth();
      await signOut(auth);
      onClose();
    } catch (error) {
      console.error('Error al cerrar la sesión');
    }
  }

  return (
    <div className={`menu-principal ${isOpen ? 'menu-open' : ''}`}>
      <div className="menu-content">
        {/* <div className="close-icon" onClick={onClose}>
          <FaBars />
        </div> */}

        {user ? (
          <>
            <Link to="/terratrak/Perfil" onClick={onClose} className="menu-link">
                Perfil
            </Link>
            <Link to="/favoritos" onClick={onClose} className="menu-link">
                Terrenos en favoritos
            </Link>
            <Link to="/terratrak/MisTerrenos" onClick={onClose} className="menu-link">
                Mis terrenos
            </Link>
            <Link to="/terratrak/PublicarTerreno" onClick={onClose} className="menu-link">
                Agregar Terreno
            </Link>
            <Link to="/" onClick={handleLogOut} className="menu-link">
                Cerrar sesión
            </Link>
          </>
          ) : (
            <>
              <Link to={"/terratrak/IniciarSesion"} onClick={onClose} className="menu-link" >
                Iniciar Sesión
              </Link>
              <Link to={"/terratrak/Registro"} onClick={onClose} className="menu-link" >
                Registrarse
              </Link>
            </>
          )}
      </div>
    </div>
  );
};

export default MenuPrincipal;
