import React, { useState } from 'react';
import '../styles/Search.css';

const Search = (props) => {
  const [searchTerm, setSearchTerm] = useState('');

  const handleSearch = () => {
    props.onChange(searchTerm);
  };

  const handleKeyDown = (e) => {
    if (e.key === 'Enter') {
      handleSearch();
    }
  };

  return (
    <div className="input-buscador">
      <input
        type="text"
        placeholder="Buscar..."
        value={searchTerm}
        onChange={(e) => setSearchTerm(e.target.value)}
        onKeyDown={handleKeyDown}
        className="custom-input"
      />
      <button type="button" onClick={handleSearch} className="custom-button">
        Buscar
      </button>
    </div>
  );
};

export default Search;

