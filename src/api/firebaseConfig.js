// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from 'firebase/analytics'
import { getMessaging } from 'firebase/messaging'

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyC46I-N4_Pn3gVBY2jqtC7s_q8tQsbMEC0",
  authDomain: "terratrack-62306.firebaseapp.com",
  projectId: "terratrack-62306",
  storageBucket: "terratrack-62306.appspot.com",
  messagingSenderId: "709519220323",
  appId: "1:709519220323:web:f5ddcda1a6af61f4d92256",
  measurementId: "G-6Q3NJNYKYF"
};

// Initialize Firebase
// export const app = initializeApp(firebaseConfig);
// const analytics = getAnalytics(app);
// export const messaging = getMessaging(app);

// Inicializa Firebase
export const app = initializeApp(firebaseConfig);
let messaging = null;
if (process.env.NODE_ENV !== 'test') {
  messaging = getMessaging(app);
}

export { messaging };  