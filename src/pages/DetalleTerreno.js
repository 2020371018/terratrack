import React, { useEffect, useState } from 'react';
import '../styles/DetalleTerreno.css';
import { useParams } from 'react-router-dom';
import { doc, getDoc, getFirestore } from 'firebase/firestore';
import { app } from '../api/firebaseConfig';

const DetalleTerreno = () => {

  const { id } = useParams();

  const [terreno, setTerreno] = useState();

  const db = getFirestore(app);

  useEffect(() => {
    const fetchTerreno = async () => {

      try {
        const docRef = doc(db, 'terrenos', id);
        const docSnap = await getDoc(docRef);

        if (docSnap.exists()) {
          setTerreno(docSnap.data());
        } else {
          alert('Ocurrio un error!')
          window.location.href = '/terratrak/Welcome';
        }
      } catch (error) {
        console.error('Error fetching', error);
      }
    }

    fetchTerreno();
  }, [id])

  return (
    <div className="detalle-terreno-container">
      <div className="detalle-terreno">
      {terreno ? (
        <>
          <div className="imagen-terreno">
            <img
              src={terreno.urlFoto || ""}
              alt={`Foto de ${terreno.nombreTerreno || ""}`}
            />
          </div>
          <div className="informacion-terreno">
            <h2>{terreno.nombreTerreno || ""}</h2>

            <div className="ubicacion">
              <h3>Ubicación:</h3>
              <p>Dirección: {terreno.direccion || ""}</p>
              <p>Coordenadas GPS: {`${terreno.coordenadas.lat}, ${terreno.coordenadas.lng}`|| ""}</p>
            </div>

            <div className="descripcion">
            <h3>Descripción del Terreno:</h3>
            <p>{terreno.descripcion || ""}</p>
          </div>

          <div className="area-servicios">
            <h3>Área y Servicios:</h3>
            <p>Área Total (m²): {terreno.areaTotal || ""} m²</p>
            <p>Servicios: {terreno.servicios || ""}</p>
          </div>

          <div className="documentacion">
            <h3>Documentación:</h3>
            <p>Escrituras: {terreno.escrituras || ""  ? "Sí" : "No"}</p>
            <p>Título de Propiedad: {terreno.tituloPropiedad || "" ? "Sí" : "No"}</p>
          </div>

          <div className="precio">
            <h3>Precio:</h3>
            <p>${terreno.precioTerreno || ""}</p>
          </div>

          <div className="notas-adicionales">
            <h3>Notas Adicionales:</h3>
            <p>{terreno.notasAdicionales || ""}</p>
          </div>
          </div>
        </>
      ) : (
        <p>Cargando terreno...</p>
      )}
      </div>
    </div>
  );
};

export default DetalleTerreno;
