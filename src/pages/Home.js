// Home.js
import React, { useEffect, useState } from 'react';
import TerrenoCard from '../components/TerrenoCard';
import '../styles/Home.css';
import { getFirestore, collection, query, where, onSnapshot } from 'firebase/firestore';
import { app, messaging } from '../api/firebaseConfig';
import { getToken, onMessage } from 'firebase/messaging'
import { Link } from 'react-router-dom';
import NoResults from '../components/NoResults';
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'


const Home = ({ searchTerm, user }) => {

  const getTokenNotification = async () => {
  const token = await getToken(messaging, {
      vapidKey: 'BPZnzs5GRCga2jKgE5MExniQKhhEY_--YNSMXXHi8MLvm5FV4GW1PqP3G5_iwtQw_6chYZ0JO3NFiXyMZMJr0Mc'
    }).catch((err) => console.log('No se pudo obtener el token', err))

    if (token) {
      console.log('Token', token);
    } if (!token) {
      console.log('No hay token disponible');
    }
  }

  const notificarme = () => {
    if (!window.Notification) {
      console.log('Este navegador no soporta notificaciones');
      return;
    }

    if (Notification.permission === 'granted') {
      getTokenNotification(); // Obtener y mostrar el token en la consola
    } else if (Notification.permission !== 'denied' || Notification.permission === 'default') {
      Notification.requestPermission((permission) => {
        console.log(permission);
        if (permission === 'granted') {
          getTokenNotification();
        } 
      })
    }
  }
  notificarme();

  useEffect(() => {
    getTokenNotification();
    onMessage(messaging, message => {
      console.log('onMessage:', message);
      toast(message.notification.title);
    })
  })

  const [terrenos, setTerrenos] = useState([]);
  const [resultados, setResultados] = useState([]);

  useEffect(() => {
    const fetchTerrenos = async () => {

      const db = getFirestore(app);
      const terrenosCollection = collection(db, 'terrenos');
      const q = query(terrenosCollection, where('activo', '==', true));
      
      const unsubscribeSnapshot = onSnapshot(q, (snapshot) => {
      const terrenosData = snapshot.docs.map((doc) => ({ id: doc.id, ...doc.data() }));
        setTerrenos(terrenosData);
      });

      return () => {
        unsubscribeSnapshot();
      }
    };

    fetchTerrenos();

  }, [])

  useEffect(() => {

    const filteredTerrenos = terrenos.filter((terreno) =>
    (!searchTerm || 
      (
        terreno.nombreTerreno && terreno.nombreTerreno.toLowerCase().includes(searchTerm.toLowerCase()) ||
        terreno.descripcion && terreno.descripcion.toLowerCase().includes(searchTerm.toLowerCase()) ||
        terreno.direccion && terreno.direccion.toLowerCase().includes(searchTerm.toLowerCase()) ||
        terreno.notasAdicionales && terreno.notasAdicionales.toLowerCase().includes(searchTerm.toLowerCase()) ||
        (terreno.precioTerreno && parseFloat(terreno.precioTerreno) <= parseFloat(searchTerm)) ||
        terreno.servicios && terreno.servicios.toLowerCase().includes(searchTerm.toLowerCase()) 
      )
    )
  );
  

    setResultados(filteredTerrenos)

  }, [searchTerm, terrenos])

  return (
    <section className='main-container-home'>

      <ToastContainer />

      {/* <h1 className='title-home'>Lista de Terrenos</h1> */}
      {resultados.length === 0 && (
         <NoResults message="No se encontraron resultados. Intente con otra búsqueda." />
      )}
      <div className='cards-container-home'>
        {resultados.map((terreno) => (
          <Link to={`/terratrak/DetalleTerreno/${terreno.id}`} key={terreno.id} >
            <TerrenoCard key={terreno.id} {...terreno} className='terreno-card' user={user} />
          </Link>
        ))}
      </div>
    </section>
  );
};

export default Home;