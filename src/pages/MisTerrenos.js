import React, { useEffect, useState } from 'react';
import CardMisTerrenos from '../components/CardMisTerrenos';
import '../styles/MisTerrenos.css';
import BotonTerrenos from '../components/BotonTerrenos';
import Search from '../components/Search';
import { getFirestore, collection, getDocs, query, where, onSnapshot } from 'firebase/firestore';
import { app } from '../api/firebaseConfig';
import { getAuth } from 'firebase/auth';
import NoResults from '../components/NoResults';

const MisTerrenos = () => {

  const [terrenos, setTerrenos] = useState([]);
  const [resultados, setResultados] = useState([]);
  const [searchTerm, setSearchTerm] = useState('');

  const handleSearchChange = (term) => {
    setSearchTerm(term)
  }

  useEffect(() => {

    const auth = getAuth(app);
    const unsubscribe = auth.onAuthStateChanged(async (user) => {
      if (user) {
        const uid = user.uid;
        const db = getFirestore(app);
        const userDocRef = collection(db, 'terrenos');
        const q = query(collection(db, 'terrenos'), where('user_uid', '==', uid), where('activo', '==', true));
        
        // onSnapshot para escuchar cambios en tiempo real
        const unsubscribeSnapshot = onSnapshot(q, (snapshot) => {
          const terrenosData = snapshot.docs.map((doc) => ({ id: doc.id, ...doc.data() }));
          setTerrenos(terrenosData);
        });

        return () => {

          // Detener la escucha cuando el componente se desmonte
          unsubscribeSnapshot();
        }
      }
    });

      return () => unsubscribe();
  }, [])  

  useEffect(() => {

    const filteredTerrenos = terrenos.filter((terreno) =>
      (!searchTerm || 
        (
          terreno.nombreTerreno && terreno.nombreTerreno.toLowerCase().includes(searchTerm.toLowerCase()) ||
          terreno.descripcion && terreno.descripcion.toLowerCase().includes(searchTerm.toLowerCase()) ||
          terreno.direccion && terreno.direccion.toLowerCase().includes(searchTerm.toLowerCase()) ||
          terreno.notasAdicionales && terreno.notasAdicionales.toLowerCase().includes(searchTerm.toLowerCase()) ||
          terreno.precioTerreno && terreno.precioTerreno.toLowerCase().includes(searchTerm.toLowerCase()) ||
          terreno.servicios && terreno.servicios.toLowerCase().includes(searchTerm.toLowerCase()) 
        )
      )
    );

    setResultados(filteredTerrenos)

  }, [searchTerm, terrenos])

  return (
    <>
      <section className='main-container-misterrenos'>
       <div className='title-bar'>
          <h1 className='title-misterrenos'>Mis terrenos</h1>
         
          <div className="corner-container">
            <a href="/terratrak/PublicarTerreno">
                <BotonTerrenos className="publicar-terreno" texto="+ Publicar terreno" tamano="grande" color="#fa6900" />
            </a>
          </div>
        </div>
        <Search onChange={handleSearchChange} />
        {resultados.length === 0 && (
              <NoResults message="Aun no has publicado nada." />
          )}
        <div className='cards-container-misterrenos'> 
          {resultados.map((terreno, index) => (
            <div key={terreno.id} className='card-container-row'>
              {terreno.activo && <CardMisTerrenos {...terreno} className='terreno-card' />}
              {index % 2 === 0 && <div className='spacer'></div>} 
            </div>
          ))}
        </div>
      </section>
    </>
  );
};

export default MisTerrenos;