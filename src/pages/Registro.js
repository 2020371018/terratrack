import React, { useState, useEffect } from 'react';
import '../styles/Registro.css';
import logoApp from '../assets/logoApp1.png';
import { app } from '../api/firebaseConfig';
import { getAuth, createUserWithEmailAndPassword, updateProfile } from 'firebase/auth';
import { getFirestore, doc, setDoc } from 'firebase/firestore';
import Alert from '../components/Alert';

const Registro = () => {

    const [alerta, setAlerta] = useState(null);

    const [nombre, setNombre] = useState("");
    const [apellidos, setApellidos] = useState("");
    const [telefono, setTelefono] = useState("");
    const [pass, setPass] = useState("");
    const [email, setEmail] = useState("");
    const [loading, setLoading] = useState(false);
    const [isOnline, setIsOnline] = useState(navigator.onLine);

    const auth = getAuth(app);
    const db = getFirestore(app);

    const mostrarAlerta = (titulo, mensaje, tipo) => {
      setAlerta({ titulo, mensaje, tipo });
    };

    const cerrarAlerta = () => {
      setAlerta(null);
    };

    useEffect(() => {
        const handleOnlineStatus = async () => {
          setIsOnline(true);

          const storedData = localStorage.getItem('formData');
          const storedData2 = localStorage.getItem('data');

          if (storedData) {
            const formData = JSON.parse(storedData);
            const formData2 = JSON.parse(storedData2);
            localStorage.removeItem('formData');
            localStorage.removeItem('data');
            await registerUser(formData, formData2);
          }
        };
        const handleOfflineStatus = () => setIsOnline(false);

        window.addEventListener('online', handleOnlineStatus);
        window.addEventListener('offline', handleOfflineStatus);

        return () => {
            window.removeEventListener('online', handleOnlineStatus);
            window.removeEventListener('offline', handleOfflineStatus);
        };
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();

        setLoading(true);

        const data = {
          correo: email,
          password: pass,
        }

        try {

          const usuario = {
            uid: null,
            nombre: nombre,
            apellidos: apellidos,
            telefono: telefono
        };

            if (!isOnline) {

              localStorage.setItem('formData', JSON.stringify(usuario));
              localStorage.setItem('data', JSON.stringify(data));

              mostrarAlerta('Aviso', 'Los datos se han guardado localmente hasta que se restablezca el internet', 'info');

              return;
            } else {
              await registerUser(usuario, data);
            }

        } catch (error) {
            console.error('Error al registrar usuario (cliente):', error);

            // Manejar el error específico de correo electrónico ya en uso
            if (error.code === 'auth/email-already-in-use') {
                alert('La dirección de correo electrónico ya está en uso. Por favor, utiliza otra.');
            } else {
                alert('Hubo un problema al registrar usuario. Por favor, inténtalo de nuevo.');
            }
        } finally {
            // Cambia el estado de "cargando" incluso si no hay conexión
            setLoading(false);
        }
    }

    async function registerUser(usuario, data) {

      if (data.correo && data.password) {
        try {
          const userCredential = await createUserWithEmailAndPassword(auth, data.correo, data.password);
  
          await updateProfile(auth.currentUser, {
            displayName: `${nombre} ${apellidos}`,
          });
  
          const uid = userCredential.user.uid;
          usuario.uid = uid;
  
          await setDoc(doc(db, "users", uid), usuario);
  
          mostrarAlerta('Éxito', 'Usuario registrado correctamente!', 'success');
  
          window.location.href = "/";
        } catch (error) {
          console.error('Error al crear cuenta', error)
        }
      } else {
        console.log('email and password no tienen valor:', data.correo, data.password);
        return;
      }      
    }

    return (
      <>
        <div className="registro-container">
            <div className="imagen-container">
                <img src={logoApp} alt="Descripción de la imagen" className="logo" />
            </div>
            <div className="formulario-container">
                <h2>Registro</h2>
                <p>Registra tus datos para ser parte de este gran sitio y obtener todos sus beneficios.</p>

                <form>
                    <label htmlFor="nombre">Nombre:</label>
                    <input type="text" id="nombre" name="nombre" onChange={(e) => setNombre(e.target.value)} />

                    <label htmlFor="apellidos">Apellidos:</label>
                    <input type="text" id="apellidos" name="apellidos" onChange={(e) => setApellidos(e.target.value)} />

                    <label htmlFor="telefono">Teléfono:</label>
                    <input type="text" id="telefono" name="telefono" onChange={(e) => setTelefono(e.target.value)} />

                    <label htmlFor="correo">Correo:</label>
                    <input type="email" id="correo" name="correo" onChange={(e) => setEmail(e.target.value)} required />

                    <label htmlFor="contrasenia">Contraseña:</label>
                    <input type="password" id="contrasenia" name="contrasenia" onChange={(e) => setPass(e.target.value)} required />

                    <div className="botones-container">
                        <button type="button" className="registro-button" onClick={handleSubmit} disabled={loading}>
                            {loading ? 'Cargando...' : 'Registrarse'}
                        </button>
                        <button type="button" className="iniciar-sesion-button">Iniciar Sesión</button>
                    </div>
                </form>
            </div>
        </div>
        {/* Mostrar la alerta si existe */}
      {alerta && (
        <Alert
          titulo={alerta.titulo}
          mensaje={alerta.mensaje}
          tipo={alerta.tipo}
          onClose={cerrarAlerta}
        />
      )}
      </>

    );
};

export default Registro;