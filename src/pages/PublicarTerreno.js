import React, { useState, useEffect } from 'react';
import '../styles/PublicarTerreno.css'
import { useNavigate } from 'react-router-dom';
import { app } from '../api/firebaseConfig';
import { getFirestore, collection, addDoc } from 'firebase/firestore';
import { getAuth } from 'firebase/auth';
import { getStorage, ref, uploadBytes, getDownloadURL } from 'firebase/storage'
import Alert from '../components/Alert';
import PouchDB from 'pouchdb';

const PublicarTerreno = () => {
  const [alerta, setAlerta] = useState(null);

  const [terrenoPublicado, setTerrenoPublicado] = useState(false);
  const localDB = new PouchDB('terrenosLocal');
  const [isOnline, setIsOnline] = useState(navigator.onLine);

  const [locationAddress, setLocationAddress] = useState('');
  const [coordinates, setCoordinates] = useState({ lat: 20.65317, lng: -100.00678 });
  const [nombreTerreno, setNombreTerreno] = useState('');
  const [areaTotal, setAreaTotal] = useState('');
  const [servicios, setServicios] = useState('');
  const [descripcion, setDescripcion] = useState('');
  const [escrituras, setEscrituras] = useState(false);
  const [tituloPropiedad, setTituloPropiedad] = useState(false);
  const [valorTerreno, setValorTerreno] = useState('');
  const [observaciones, setObservaciones] = useState('');

  const db = getFirestore(app);
  const auth = getAuth(app);

  const navigate = useNavigate();

  const mostrarAlerta = (titulo, mensaje, tipo) => {
    setAlerta({ titulo, mensaje, tipo });
  };

  const cerrarAlerta = () => {
    setAlerta(null);
  };

  const navigateToWelcome = () => {
    navigate('/terratrak/Welcome');
  };

  let uid;
  auth.onAuthStateChanged((user) => {
    if (user) {
      uid = user.uid
    }
  });

  const uploadPhoto = async (file) => {
    const storage = getStorage(app);
    const storageRef = ref(storage, `terrenos/${uid}/${file.name}`);
    await uploadBytes(storageRef, file);
    const photoURL = await getDownloadURL(storageRef);
    return photoURL;
  }

  const handleFormSubmit = async (e) => {
    e.preventDefault();

    // Obtener foto del formulario
    const fotoInput = document.getElementById('foto');
    const fotoFile = fotoInput.files[0];

    const formData = {
      nombreTerreno: nombreTerreno,
      urlFoto: null,
      direccion: locationAddress,
      coordenadas: coordinates,
      areaTotal: Number(areaTotal),
      servicios: servicios,
      descripcion: descripcion,
      escrituras: escrituras,
      tituloPropiedad: tituloPropiedad,
      precioTerreno: Number(valorTerreno),
      notasAdicionales: observaciones,
      user_uid: uid,
      activo: true,
    };

    // Verificar conexión a internet
    if (isOnline) {
      if (fotoFile) {
        const photoURL = await uploadPhoto(fotoFile);
        formData.urlFoto = photoURL;
      }

      try {
        const terreno =  await addDoc(collection(db, 'terrenos'), formData); 
        if (terreno) {
          mostrarAlerta('Éxito', 'Terreno publicado exitosamente!', 'success');
          navigateToWelcome()
        }
        
      } catch (error) {
        console.error(error);
      }
       
    } else {

      // Almacenar en localStorage
      localStorage.setItem('formData', JSON.stringify(formData));
      if (fotoFile) {
        await guardarImagenLocalmente(fotoFile);
      }
      mostrarAlerta('Aviso', 'Los datos se han guardado localmente hasta que se restablezca el internet', 'info');
    }
  };

  async function guardarImagenLocalmente(file) {

    const nombreArchivo = file.name;
    const tipoArchivo = file.type;
    const uid = new Date().toISOString();;

    const data = {
      nombre: nombreArchivo,
      tipo: tipoArchivo,
      id: uid
    }

    const jsonData = JSON.stringify(data);

    localStorage.setItem('fotoDetails', jsonData);

    try {
      await localDB.putAttachment(uid, nombreArchivo, file, tipoArchivo);
    } catch (error) {
      console.error('Error al guardar la imgane en PouchDB', error);
    }
  }

  async function recuperarImagenDePouchDB(id, name, type) {
    try {
      const response = await localDB.getAttachment(id, name);
      const blob = new Blob([response], { type: type });
      const file = new File([blob], name, { type: type });
      
      return file;
    } catch (error) {
      console.error('Error al recuperar la imagen', error)
    }
  }

  const handleEscriturasChange = (e) => {
    setEscrituras(e.target.checked);
  }

  const handleTituloPropiedadChange = (e) => {
    setTituloPropiedad(e.target.checked);
  }

  useEffect(() => {
    initMap();

    const handleOnline = async () => {
      setIsOnline(true);

      if (!terrenoPublicado) {
        const storedData = localStorage.getItem('formData');
        const fotoFile = localStorage.getItem('fotoDetails');

        if (storedData) {
          const formData = JSON.parse(storedData);

          if (fotoFile) {
            const fotoData = JSON.parse(fotoFile);

            const response = await recuperarImagenDePouchDB(fotoData.id, fotoData.nombre, fotoData.tipo);
            
            try {
              const photoURL = await uploadPhoto(response);
              formData.urlFoto = photoURL;

              const terrenosRef = collection(db, 'terrenos');
              await addDoc(terrenosRef, formData);
            } catch (error) {
              console.error('Error al subir la imagen', error)
            }
            
            localStorage.removeItem('fotoDetails');

            mostrarAlerta('Éxito', 'Terreno publicado exitosamente!', 'success');
            navigateToWelcome();
          }

          localStorage.removeItem('formData');
          setTerrenoPublicado(true);
        }
      }
    };

    const handleOffline = () => {
      setIsOnline(false);
    }

    window.addEventListener('online', handleOnline);
    window.addEventListener('offline', handleOffline);

    return () => {
      window.removeEventListener('online', handleOnline);
      window.removeEventListener('offline', handleOffline);
    }
    
  }, []);

  const initMap = () => {

    const map = new window.google.maps.Map(document.getElementById('map'), {
      center: coordinates,
      zoom: 15,
    })

    const geocoder = new window.google.maps.Geocoder();

    const tempMarker = new window.google.maps.Marker({
      position: coordinates,
      map,
      draggable: true,
    });

    tempMarker.addListener('drag', () => {
      const newPosition = tempMarker.getPosition();
      const newCoordinates = {
        lat: newPosition.lat().toFixed(5),
        lng: newPosition.lng().toFixed(5),
      };

      document.getElementById('coordenadas').value = `(${newCoordinates.lat}, ${newCoordinates.lng})`;
      setCoordinates(newCoordinates);
      
      geocoder.geocode({ location: newPosition }, (results, status) => {
        if (status === 'OK' && results[0]) {
          const address = results[0].formatted_address;
          document.getElementById('direccion').value = address;
          setLocationAddress(address);
        } else {
          console.error('Error en obtener la direccion', status)
        }
      })
    });

    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        const pos = {
          lat: position.coords.latitude,
          lng: position.coords.longitude,
        }
        setCoordinates(pos);
        map.setCenter(pos);
        tempMarker.setPosition(pos)
      }, () => {
        alert('Error al obtener la ubicación');
      })
    } else {
      alert('El navegador no admite geolocalización')
    }
  }

  return (
    <>
      <form className="formulario-publicacion-terrenos"  onSubmit={navigateToWelcome} >
        <h2>Formulario de Publicación de Terrenos</h2>

        {/* Información Básica */}
        <div className="form-section">
            <h3>Información Básica</h3>
            <div className="input-group">
                <div className="form-field">
                    <label htmlFor="nombreTerreno">Nombre del Terreno</label>
                    <input type="text" id="nombreTerreno" name="nombreTerreno" onChange={(e) => setNombreTerreno(e.target.value)} required />
                </div>
                <div className="form-field">
                    <label htmlFor="foto">Foto</label>
                    <input type="file" id="foto" name="foto" accept="image/*" />
                </div>
                <div className="form-field">
                    <label htmlFor="direccion">Dirección</label>
                    <input type="text" id="direccion" name="direccion" required value={locationAddress} onChange={(e) => setLocationAddress(e.target.value)} />
                </div>
                <div className="form-field">
                    <label htmlFor="coordenadas">Coordenadas GPS</label>
                    <input type="text" id="coordenadas" name="coordenadas" required />
                </div>
            </div>
            <div className='form-section'>
              <h3>Ubicacion del Terreno</h3>
              <div id='map' style={{ height: '300px', marginBottom: '20px' }}></div>
            </div>
        </div>

        {/* Descripción del Terreno */}
        <div className="form-section">
          <h3>Descripción del Terreno</h3>
            <div className="input-group">
                <div className="form-field">
                    <label htmlFor="areaTotal">Área Total (m²)</label>
                    <input type="number" id="areaTotal" name="areaTotal" onChange={(e) => setAreaTotal(e.target.value)} required />
                </div>
                <div className="form-field">
                    <label htmlFor="servicios">Servicios con los que cuenta</label>
                    <input type="text" id="servicios" name="servicios" onChange={(e) => setServicios(e.target.value)} required />
                </div>
            </div>
          <label htmlFor="descripcion">Breve descripción del terreno</label>
          <textarea id="descripcion" name="descripcion" rows="4" onChange={(e) => setDescripcion(e.target.value)} required></textarea>
        </div>

        {/* Documentación */}
        <div className="form-section">
          <h3>Documentación</h3>
          <label className="checkbox-label"><input type="checkbox" name="escrituras" checked={escrituras} onChange={handleEscriturasChange} /> Escrituras</label>
          <label className="checkbox-label"><input type="checkbox" name="tituloPropiedad" checked={tituloPropiedad} onChange={handleTituloPropiedadChange} /> Título de Propiedad</label>
        </div>

        {/* Precio */}
        <div className="form-section">
          <h3>Precio</h3>
          <label htmlFor="valorTerreno">Valor Actual del Terreno</label>
          <input type="number" id="valorTerreno" name="valorTerreno" onChange={(e) => setValorTerreno(e.target.value)} required />
        </div>

        {/* Notas Adicionales */}
        <div className="form-section">
          <h3>Notas Adicionales</h3>
          <label htmlFor="observaciones">Observaciones o Comentarios</label>
          <textarea id="observaciones" name="observaciones" onChange={(e) => setObservaciones(e.target.value)} rows="4"></textarea>
        </div>

        <button type="button" onClick={handleFormSubmit}>Publicar Terreno</button>
      </form>
      

      {/* Mostrar la alerta si existe */}
      {alerta && (
        <Alert
          titulo={alerta.titulo}
          mensaje={alerta.mensaje}
          tipo={alerta.tipo}
          onClose={cerrarAlerta}
        />
      )}
    </>
  );
};

export default PublicarTerreno;