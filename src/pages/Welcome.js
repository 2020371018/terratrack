import React, { useEffect, useState } from 'react';
import Home from '../pages/Home';
import '../styles/Welcome.css';
import BotonTerrenos from '../components/BotonTerrenos';
import Search from '../components/Search';
import { getAuth, onAuthStateChanged } from 'firebase/auth';
import { app } from '../api/firebaseConfig'; 

const Welcome = () => {
  const [searchTerm, setSearchTerm] = useState('');
  const [user, setUser] = useState(null);

  const handleSearchChange = (term) => {
    setSearchTerm(term)
  }

  useEffect(() => {
    const auth = getAuth(app);
    const unsubscribe = onAuthStateChanged(auth, (user) => {
      if (user) {
        setUser(user)
      }
    });

    return () => unsubscribe();
  }, [])

  return (
    <>
        <div className="corner-container">
          <a href="/terratrak/PublicarTerreno">
            <BotonTerrenos texto="+ Publicar terreno" tamano="grande" color="#fa6900" />
            </a>
        </div>
        <Search onChange={handleSearchChange} />
        <div className="home-container">
          <Home searchTerm={searchTerm} user={user} />
        </div>
    </>
  );
};

export default Welcome;