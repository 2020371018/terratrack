import React from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Registro from '../pages/Registro';
import Layout from '../containers/Layout';
import Home from '../pages/Home';
import IniciarSesion from '../pages/IniciarSesion';
import DetalleTerreno from '../pages/DetalleTerreno';
import Welcome from '../pages/Welcome';
import PublicarTerreno from '../pages/PublicarTerreno';
import MisTerrenos from '../pages/MisTerrenos';
import EditarTerreno from '../pages/EditarTerreno';
import Perfil from '../pages/Perfil';


const RutasConLayout = () => {
  return (
    <Layout>
      <Routes>
        <Route path='/' element={<Home />} />
        <Route path='/Welcome' element={<Welcome />} />
        <Route path='/PublicarTerreno' element={<PublicarTerreno />} />
        <Route path='/MisTerrenos' element={<MisTerrenos />} />
        <Route path='/EditarTerreno/:id' element={<EditarTerreno />} />
        <Route path='/Perfil' element={<Perfil />} />
        <Route path='/Registro' element={<Registro />} />
        <Route path='/IniciarSesion' element={<IniciarSesion />} />
        <Route path='/DetalleTerreno/:id' element={<DetalleTerreno />} />
      </Routes>
    </Layout>
  );
}

const Navegation = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path='/*' element={<RutasConLayout />} />
        <Route path='/terratrak/*' element={<RutasConLayout />} />
      </Routes>
    </BrowserRouter>
  );
}

export default Navegation;
