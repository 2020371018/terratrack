// const admin = require('firebase-admin');
// const firebase = require('firebase/app');
// require('firebase/firestore')
const admin = require('firebase-admin')

const serviceAccount = require('../service/terratrack-62306-firebase-adminsdk-8uh8v-6ea49a5b30.json');

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
});

const adminDb = admin.firestore();
const db = admin.firestore();

module.exports = {
    db,
    adminDb,
};