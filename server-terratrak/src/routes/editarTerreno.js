const express = require('express');
const { db } = require('../firebaseConfig');

const router = express.Router();

// Editar un terreno existente
router.put('/:id', async (req, res) => {
    const { id } = req.params;
    const dataToUpdate = req.body;

    console.log(req)

    try {
        const terrenoRef = db.collection('terrenos').doc(id);
        await terrenoRef.update(dataToUpdate);

        res.json({ message: 'Terreno actualizado correctamente' });
    } catch (error) {
        console.error('Error al actualizar el terreno', error);
        res.status(500).json({ error: 'Error interno del servidor' });
    }
});

module.exports = { router };