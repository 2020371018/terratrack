const express = require('express');
const { db } = require('../firebaseConfig');

const router = express.Router();


// get a single terrain
router.get('/:id', (req, res) => {
    const { id } = req.params;

    try {

        var docRef = db.collection('terrenos').doc(id);

        docRef.get().then((doc) => {
            if (doc.exists) {
                const terreno = doc.data();
                res.json(terreno);
            } else {
                res.status(404).json({ error: 'Terreno no encontrado' });
            }
        }).catch((error) => {
            console.error('Error al obtener los detalles del terreno:', error);
            res.status(500).json({ error: 'Error interno del servidor' });
        })

    } catch (error) {
        console.error('Error al obtener los detalles del terreno:', error);
        res.status(500).json({ error: 'Error interno del servidor' });
    }
});

// get all active terrains
router.get('/', async (req, res) => {
    try {
        const terrenosRef = db.collection('terrenos');
        const snapshot = await terrenosRef.where('activo', '==', true).get();

        const terrenos = snapshot.docs.map((doc) => ({
            id: doc.id,
            ...doc.data()
        }));

        res.json(terrenos);
    } catch (error) {
        console.error('Error al obtener la lista de terrenos', error);
        res.status(500).json({ error: 'Error interno del servidor' })
    }
})
module.exports = { router };