const express = require('express');
const cors = require('cors');
const { adminDb } = require('./firebaseConfig');
const { router: terrenosRouter } = require('./routes/terrenos');
const { router: editarTerrenoRouter} = require('./routes/editarTerreno');

const app = express();
const port = 3000;

// Middleware CORS
app.use(cors());

// Verificar la conexión a Firebase
(async () => {
    try {
        await adminDb.listCollections();
        console.log('Conectado a Firebase');
    } catch (error) {
        console.error('Error al conectar a Firebase', error);
        process.exit(1);
    }
})();

// Middleware para todas las rutas relacionadas con terrenos
app.use('/api/terrenos', terrenosRouter);
app.use('/api/editarTerreno', editarTerrenoRouter);

// Ruta raíz
app.get('/', (req, res) => {
    res.send('Hola Mundo!');
});

// Arrancar el servidor
app.listen(port, () => {
    console.log(`Servidor en ejecución en http://localhost:${port}`);
});