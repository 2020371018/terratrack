const db = new PouchDB('terrenos');

// Postear mensajes a la API
function postearMensajes() {
    const posteos = [];

    return db.allDocs({ include_docs: true }).then(docs => {
        docs.rows.forEach(row => {
            const doc = row.doc;

            const fetchPom = fetch('api/note', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(doc)
            }).then(res => {
                return db.remove(doc)
            });

            posteos.push(fetchPom);
        })

        return Promise.all(posteos);
    })
}