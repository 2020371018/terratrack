importScripts('https://www.gstatic.com/firebasejs/10.6.0/firebase-app-compat.js')
importScripts('https://www.gstatic.com/firebasejs/10.6.0/firebase-messaging-compat.js')

const firebaseConfig = {
    apiKey: "AIzaSyC46I-N4_Pn3gVBY2jqtC7s_q8tQsbMEC0",
    authDomain: "terratrack-62306.firebaseapp.com",
    projectId: "terratrack-62306",
    storageBucket: "terratrack-62306.appspot.com",
    messagingSenderId: "709519220323",
    appId: "1:709519220323:web:f5ddcda1a6af61f4d92256",
    measurementId: "G-6Q3NJNYKYF"
  };

// Initialize Firebase
const app = firebase.initializeApp(firebaseConfig);
const messaging = firebase.messaging(app);

messaging.onBackgroundMessage(payload => {
    console.log('Recibiendo mensaje en segundo plano');
    const tituloNotificacion = payload.notification.title;
    const options = {
        body: payload.notification.body,
        icon: '../img/40.png'
    }

    self.registration.showNotification(tituloNotificacion, options);
})